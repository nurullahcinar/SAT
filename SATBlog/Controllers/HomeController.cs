﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SATBlog.Models;

namespace SATBlog.Controllers
{
    public class HomeController : Controller
    {
        AdminDataEntities db = new AdminDataEntities();
        // GET: Home
        public ActionResult Index()
        {
            return View(db.BlogText.ToList());
        }
    }
}